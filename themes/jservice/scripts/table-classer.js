hexo.extend.tag.register('bulmatable', async (args, content) => {
    const rendered = await hexo.render.render({ text: content, engine: 'md' });
    const classes = ['table'];
    if (args.some(x => x === 'fullwidth')) classes.push('is-fullwidth');
    if (args.some(x => x === 'striped')) classes.push('is-striped');
    if (args.some(x => x === 'bordered')) classes.push('is-bordered');
    if (args.some(x => x === 'narrow')) classes.push('is-narrow');
    if (args.some(x => x === 'hoverable')) classes.push('is-hoverable');
    return rendered.replace('<table>', `<table class="${classes.join(' ')}">`);
}, { ends: true, async: true });