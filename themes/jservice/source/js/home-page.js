const url = 'http://localhost:8080/rest/clues/random';
const fieldPrefixes = {
  prompt: '',
  answer: '',
  value: '$',
  'category.title': '',
  'game.aired': ''
}

document.addEventListener('DOMContentLoaded', () => {
  document
    .querySelector('#get-a-clue')
    ?.addEventListener('click', async () => {
      const response = await fetch(url);
      if (response.ok) {
        const clue = await response.json();
        for (const field of Object.keys(fieldPrefixes)) {
          const node = document.getElementById(field);
          if (node) {
            let value = jsonpath.query(clue, `$.${field}`)[0];
            if (value.html !== undefined) {
              value = value.html;
            }
            node.innerHTML = fieldPrefixes[field] + value;
          }
        }
      }
    });
});