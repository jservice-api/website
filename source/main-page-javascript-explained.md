---
title: Main Page JavaScript Explained
date: 2024-04-12
author: Curtis Schlak
---

# Main Page JavaScript Explained

<div class="section">
<div class="message is-warning">
<div class="message-body">

This content is primarily for **front-end developers**. To get the most out of
this, you'll likely want to have a experience with **HTML** and **JavaScript**.

</div>
</div>

Using JavaScript to asynchronously load and display data is a time-honored
tradition in Web programming. If you're using <abbr title="JavaScript that
directly interacts with the DOM">Vanilla JS</abbr> like the main page has, then
you have to manage the data fetching _and_ putting that data into the HTML
elements on the page.

This page shows the HTML, then explains how the JavaScript works to turn an
empty display into stuff with text.

</div>

## The data

Let's take a look at the shape of the data that is returned.

```json
{
  "id": 51923,
  "answer": "lute",
  "question": "The guitar & mandolin are members of this ancient family of stringed instruments",
  "value": 200,
  "categoryId": 25500,
  "gameId": 933,
  "invalidCount": 0,
  "category": {
    "id": 25500,
    "href": "/api/categories/25500",
    "title": "MUSIC",
    "canon": true
  },
  "game": {
    "href": "/api/games/933",
    "aired": "1992-04-17",
    "canon": true
  },
  "canon": true
}
```

Of that content, we want to display five values:

* The date (`game.aired`)
* The category name (`category.title`)
* The prompt (`question`)
* The answer (`answer`)
* The value (`value`)

The HTML should reflect those five values.

## The HTML

This is the HTML that you see on the main page that is just waiting for some
data to show up.

```html
<dl>
  <dt class="has-text-weight-bold">Date</dt>
  <dd id="game.aired">&nbsp;</dd>

  <dt class="has-text-weight-bold">Category</dt>
  <dd id="category.title">&nbsp;</dd>

  <dt class="has-text-weight-bold">Prompt</dt>
  <dd id="question">&nbsp;</dd>

  <dt class="has-text-weight-bold">Answer</dt>
  <dd id="answer">&nbsp;</dd>

  <dt class="has-text-weight-bold">Value</dt>
  <dd id="value">&nbsp;</dd>
</dl>
```

This uses the oft-ignored [Description List
element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl). When the
data loads, the `dd` elements, each with a specific `id` attribute, are targeted
with content from the data.

You'll note that two of the `id` values have periods in them, `game.aired` and
`category.title`. These are valid `id` values according to the HTML Living Spec.
We chose to do this so that we can align the path in the content to the name of
the targeted element.

## The JavaScript

Here's the whole JavaScript. Have a look at it. Then, after it, we'll get a nice
dissection going on.

```javascript
const url = 'https://jservice.xyz/api/random-clue';
const fieldPrefixes = {
  question: '',
  answer: '',
  value: '$',
  'category.title': '',
  'game.aired': ''
}

document.addEventListener('DOMContentLoaded', () => {
  document
    .querySelector('#get-a-clue')
    ?.addEventListener('click', async () => {
      const response = await fetch(url);
      if (response.ok) {
        const clue = await response.json();
        for (const field of Object.keys(fieldPrefixes)) {
          const node = document.getElementById(field);
          if (node) {
            const value = jsonpath.query(clue, `$.${field}`)
            node.innerHTML = marked.parse(`${fieldPrefixes[field]}${value}`);
          }
        }
      }
    });
});
```

### Some constants

Who doesn't love some global immutable data, amirite? 😍 First, a URL for us
to query, then an object whose keys are paths into the result, and whose values
are prefixes that we'll use with the display (just putting a `$` in front of
the value, really).

{% codeblock lang:js %}
const url = 'https://jservice.xyz/api/random-clue';
const fieldPrefixes = {
  question: '',
  answer: '',
  value: '$',
  'category.title': '',
  'game.aired': ''
}
{% endcodeblock %}

With this setup, after receiving the data from the endpoint at `url`, we'll loop
over the keys in the `fieldPrefixes` object to extract data using
[JSONPath](https://datatracker.ietf.org/doc/id/draft-goessner-dispatch-jsonpath-00.html),
a way to extract data from a deserialized JSON response. So, the field prefix
key `question` will get the value for the `question` property, and the
`game.aired` field prefix key will get the `game` property value, then get the
`aired` property value from that.

The isomorphic (runs on the server or in the browser)
[jsonpath](https://www.npmjs.com/package/jsonpath) library is loaded in the main
page to facilitate that object querying.

### Events!

We subscribe to two DOM events in the JavaScript, the `DOMContentLoaded` event
to wait for the DOM to be ready for other event subscriptions, then the `click`
event on the button that has the id `#get-a-clue`.

{% codeblock lang:js first_line:10 %}
document.addEventListener('DOMContentLoaded', () => {
  document
    .querySelector('#get-a-clue')
    ?.addEventListener('click', async () => {
{% endcodeblock %}

The event handler for the button is an `async` function because we will use the
Promise-based [Fetch
API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) to get data
from the server.

### DATA!

Oh, here's the fun part.

{% codeblock lang:js first_line:14 %}
const response = await fetch(url);
if (response.ok) {
  const clue = await response.json();
{% endcodeblock %}

The code on the line 14 gets the `Response` object after the Promise completes.
Then, it checks if the response is "okay", that is, it checks if the status of
the response is a value from 200 through 299. If that's true, then it waits for
the body to load and get parsed into JSON on line 16.

#### Why two `await`s?

Have you ever wondered why you `await` a response from the `fetch` method (line
14), then have to `await` a second time to get the content (line 16)? That's
because the designers of the `fetch` method were _smart_!

Here's what the HTTP response looks like in full from the server.

```plain
HTTP/1.1 200 OK
Server: nginx/1.14.2
Date: Sat, 13 Apr 2024 17:41:16 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 362
Connection: keep-alive
Vary: Origin
Access-Control-Allow-Origin: http://localhost:4000
X-Frame-Options: DENY
X-Content-Type-Options: nosniff

...the full JSON payload...
```

The first `await` returns when the empty blank line on line 11 is encountered.
At that point, the code can determine things like the status code of the
response (200 or 404 or whatever), can provide you with all of the values of the
headers, and other data, like if your request was redirected.

Meanwhile, your browser is continuing to collect the data in the body of the
response: the JSON, the HTML, whatever. When you want the content of the
response, with something like `response.json()`, the browser may not have yet
received the entire body. That's what the second `await` is for: to make sure
that it has read all of the bytes before returning the content.

If the designers of the Fetch API hadn't done it this way, hadn't split up the
two calls, you would have to wait until the _entire_ body was loaded before you
could even check if the response was good or bad.

### Using the data

Now that we have the data, let's plop it into the HTML elements that we saw
above in [The HTML](#the-html) using the strategy found in [Some
constants](#some-constants).

{% codeblock lang:js first_line:17 %}
for (const field of Object.keys(fieldPrefixes)) {
  const node = document.getElementById(field);
  if (node) {
    const value = jsonpath.query(clue, `$.${field}`)
    node.innerHTML = marked.parse(`${fieldPrefixes[field]}${value}`);
  }
}
{% endcodeblock %}

Line 17 loops over each of the keys of the object stored in `fieldPrefixes`. We
could have used a
[`Map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map), but ... meh.

For each of those keys, on line 18 we get the HTML element from the DOM with the
same `id` value as the key. Aligning values in `id`s and keys like this allow us
to automate this code more generically, then having to do the data plopping one
at a time.

On line 19, we check if the DOM element exists. This is the only defensive
programming in this code, which is more on part of the laziness of the author
than it is about good code.

We use the `jsonpath` library on line 20 to pluck the value out of the object
stored in `clue`, which is the JSON parsed data from back on line 16. JSONPath
uses the `$` as the "this" reference, so a path like `$.answer` gets the value
of the `answer` property on the `clue` object, and `$.category.title` gets the
object in the `category` property, then the value of the `title` property from
it. The little string interpolation, `$.${field}` is just prepending the `$.` to
the value of the key to make it a proper path as expected by JSONPath.

Once we have the value, we run it through
[`marked`](https://www.npmjs.com/package/marked), an isomorphic Markdown
processor. The resulting HTML gets stored in the node's inner HTML.

## Simple and easy!

The data is pretty well designed. The code is easy to understand.

Now, you can go off and practice your `GET`, `POST`, `PUT`, and `DELETE` with
creating and playing random games, creating new categories, and managing clues.

Happy programming! 🥳