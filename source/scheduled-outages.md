---
title: Scheduled Outages
author: Curtis Schlak
---

# Scheduled outages

{% bulmatable fullwidth striped %}
| Date          |  From  |   To   | Service             | Reason                                         |
|---------------|:------:|:------:|---------------------|------------------------------------------------|
| 21 April 2024 | 9am CT | 9pm CT | `node.jservice.xyz` | {% post_link Version-2-Rollout "v2 Rollout" %} |
{% endbulmatable %}

