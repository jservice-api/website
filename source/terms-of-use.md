---
title: Legal Stuff
author: Curtis Schlak
date: 2024-04-01
---

# Legal Stuff

Please find below the terms of use and acceptable use policy for jservice.xyz
and its services.

## Terms of Use

Please read these terms of use ("terms of use", "agreement") carefully before
using jservice.xyz website (“website”, "service") or its affiliated services
node.jservice.xyz.

### Conditions of use

By using this website, you certify that you have read and reviewed this
Agreement and that you agree to comply with its terms. If you do not want to be
bound by the terms of this Agreement, you are advised to leave the website
accordingly. jservice.xyz only grants use and access of this website, its
products, and its services to those who have accepted its terms.

### Privacy policy

Before you continue using our website, we advise you to read our privacy policy
[link to privacy policy] regarding our user data collection. It will help you
better understand our practices.

### Age restriction

You must be at least 18 (eighteen) years of age before you can use this website.
By using this website, you warrant that you are at least 18 years of age and you
may legally adhere to this Agreement. jservice.xyz assumes no responsibility for
liabilities related to age misrepresentation.

### Intellectual property

You agree that all materials, products, and services provided on this website
are the property of jservice.xyz, its affiliates, directors, officers,
employees, agents, suppliers, or licensors including all copyrights, trade
secrets, trademarks, patents, and other intellectual property. You also agree
that you will reproduce or redistribute the source code of jservice.xyz’s
intellectual property in compliance with the MIT License. You also agree that
you will reproduce or redistribute the content of the Web site of jservice.xyz’s
intellectual property in compliance with the Creative Commons
Attribution-ShareAlike 4.0 International license. You grant jservice.xyz a
royalty-free and non-exclusive license to display, use, copy, transmit, and
broadcast the content you upload and publish. For issues regarding intellectual
property claims, you should contact the company in order to come to an
agreement.

### User accounts

As a user of this website, you may be asked to register with us and provide
private information. You are responsible for ensuring the accuracy of this
information, and you are responsible for maintaining the safety and security of
your identifying information. You are also responsible for all activities that
occur under your account or password. If you think there are any possible issues
regarding the security of your account on the website, inform us immediately so
we may address it accordingly. We reserve all rights to terminate accounts, edit
or remove content and cancel orders in their sole discretion.

### Limitation of Liability

IN NO EVENT WILL JSERVICE.XYZ BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY
DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES RESULTING FROM OR RELATED TO
USE OF THE WEBSITES OR SERVICES, OR ON ANY OTHER HYPER LINKED OR THIRD PARTY
WEBSITE, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, BUSINESS INTERRUPTION,
LOSS OF PROGRAMS OR DATA ON YOUR INFORMATION HANDLING SYSTEM OR OTHERWISE,
HOWEVER ARISING, EVEN IF WE ARE EXPRESSLY ADVISED OF THE POSSIBILITY OF SUCH
DAMAGES.

### Use at Own Risk – Disclaimer of Warranties

ALL DATA PROVIDED ON THE WEBSITES AND ANY RELATED COMMUNICATIONS ARE PROVIDED ON
AN "AS IS" BASIS FOR INFORMATION PURPOSES ONLY. IN ADDITION, ALL WARRANTIES ARE
HEREBY DISCLAIMED, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE, OR NON INFRINGEMENT. SOME JURISDICTIONS DO NOT ALLOW
THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSION MAY NOT APPLY TO
YOU. Your use of the Websites and any services are at your own risk. None of the
information or data on the Websites, including third party content has been
verified or authenticated. Any such information or data on the Websites may
contain technical inaccuracies or typographical errors. Information and data may
be changed or updated without notice. jservice.xyz may also make improvements
and/or changes in the products and/or the programs described in this information
at any time without notice. jservice.xyz shall have no liability for any errors
or omissions in such information or data, including third party content, whether
provided by jservice.xyz or others.

### No Confidential Information Accepted

jservice.xyz does not wish to receive, act to procure, nor desire to solicit,
confidential or proprietary information from you through the Websites. Please
note that any information or material divulged during chats, posted to
newsgroups or sent to jservice.xyz will be deemed NOT to be confidential by
jservice.xyz. By the submission of any information or material, you grant
jservice.xyz an unrestricted, irrevocable license to use, reproduce, display,
perform, modify, transmit and distribute those materials or information, and you
also agree jservice.xyz is free to use any idea, concept, know-how or technique
that you send to us or share with us for any purpose. However, we will not
release your name or otherwise publicize the fact that you submitted materials
or other information to us unless: (a) we obtain your permission to use your
name; or (b) we first notify you that the materials or other information you
submit to a particular part of this site will be published or otherwise used
with your name on it; or (c) we are required to do so by law.

### Local Laws and Export Control

jservice.xyz controls and operates the Websites from its headquarters in the
United States of America and the services, information and data on the Websites
may not be appropriate for use in other locations. If you use the Websites or
services outside the United States of America, you shall be responsible for
following the applicable export/import control laws and all applicable local
laws.

### General

jservice.xyz prefers to advise you of your inappropriate behavior and recommend
any necessary corrective action. However, certain violations of the Terms, as
determined by jservice.xyz, will result in termination of your access to the
Websites or services. Massachusetts law and controlling U.S. federal law,
without regard to the choice or conflicts of law provisions, will govern the
Terms. Any disputes relating to these Terms or the Site will be heard in the
state and federal courts located in the State of Texas. If any of these Terms is
found to be inconsistent with applicable to law, then such term shall be
interpreted to reflect the intentions of the parties, and no other terms will be
modified. jservice.xyz’s failure to enforce any of the Terms is not a waiver of
such term. The Terms are the entire agreement between you and jservice.xyz and
supersede all prior or contemporaneous negotiations, discussions or agreements,
written or oral, between you and jservice.xyz about the Websites. The
proprietary rights, disclaimer of warranties, indemnities, limitations of
liability and general provisions shall survive any termination of the Terms.

---

## Acceptable Use Policy

This Acceptable Use Policy (“Policy”) applies to the use of jservice.xyz
Software as a Service offerings and any other product or service from which this
Policy is linked (collectively the “SaaS”) and any other product hosted on the
jservice.xyz domain. This Policy is in addition to any other terms and
conditions pursuant to which jservice.xyz provides the SaaS to you.

jservice.xyz may make reasonable modifications to this Policy from time to time
by posting a new version of this document on the jservice.xyz website. Unless
otherwise specified, revisions are effective immediately upon posting.
Accordingly, we recommend that you visit the jservice.xyz website regularly to
ensure that your activities conform to the most recent version of this Policy.
Your continued access to, or use of, the SaaS on or after the effective date of
any changes to this Policy constitutes your acceptance of any such updates.

Questions about this Policy (e.g., whether any contemplated use is permitted)
and reports of violations of this Policy should be directed to
support@jservice.xyz.com.

You agree not to use, and not to encourage or permit any person to use, the SaaS
in a manner prohibited by this Policy. Prohibited uses and activities include,
without limitation, any use of the SaaS in a manner that, in jservice.xyz’s
reasonable judgment, involves, facilitates, or attempts any of the following:

### Conduct and Information Restrictions

* violating any law of, or committing conduct that is tortuous or unlawful in,
  any applicable jurisdiction;
* displaying, performing, sending, receiving or storing any content that is
  obscene, pornographic, lewd, lascivious, or excessively violent, regardless of
  whether the material or its dissemination is unlawful;
* advocating or encouraging violence against any government, organization,
  group, individual or property, or providing instruction, information, or
  assistance in causing or carrying out such violence, regardless of whether
  such activity is unlawful;
* uploading, posting, publishing, transmitting, reproducing, creating derivative
  works of, or distributing in any way information, software or other material
  obtained through the SaaS or otherwise that is protected by copyright, trade
  secret or other intellectual property right, without obtaining any required
  permission of the owner;
* deleting or altering author attributions, copyright notices, or trademark
  notices, unless expressly permitted in writing by the owner;
* transmitting sensitive personal information of an individual in a manner that
  can be associated with such individual, including without limitation Social
  Security number, government-issued identification number, health or medical
  information (including protected health information as defined by the Health
  Insurance Portability and Accountability Act of 1996, as amended), credit card
  or debit card number, financial account information, access codes and PINS,
  date of birth, or user account credentials.
* obtaining unauthorized access to any system, network, service, or account;
* interfering with service to any user, site, account, system, or network by use
  of any program, script, command, or otherwise;
* evading spam filters, or sending or posting a message or email with deceptive,
  absent, or forged header or sender identification information;
* transmitting unsolicited bulk or commercial messages commonly known as “spam;”
* sending large numbers of copies of the same or substantially similar messages,
  empty messages, or messages which contain no substantive content, or large
  messages or files that disrupts a server, account, blog, newsgroup, chat, or
  similar service;
* participating in the collection of large numbers of email addresses, screen
  names, or other identifiers of others (without their prior consent), a
  practice sometimes known as spidering or harvesting, or participate in the use
  of software (including “spyware”) designed to facilitate this activity;
* falsifying, altering, or removing message headers;
* using the Services to record or monitor a phone call or other communication
  without securing consent from the participants, as required under applicable
  law; or
* impersonating any person or entity, engage in sender address falsification,
  forge anyone else's digital or manual signature, or perform any other similar
  fraudulent activity (for example, “phishing”).

### Technical Restrictions

* accessing any third party computer or computer system, network, software, or
  data without their knowledge and consent; breaching the security of another
  user or system; or attempting to circumvent the user authentication or
  security of any host, network, or account. This includes, but is not limited
  to, accessing data not intended for you, logging into or making use of a
  server or account you are not expressly authorized to access, or probing the
  security of other hosts, networks, or accounts without express permission to
  do so;
* using or distributing tools or devices, material or other information,
  designed or used for compromising security or whose use is otherwise
  unauthorized, such as password guessing programs, decoders, password
  gatherers, keystroke loggers, analyzers, cracking tools, packet sniffers,
  encryption circumvention devices, viruses, ransomware, or Trojan horse
  programs;
* copying, distributing, or sublicensing any proprietary software provided in
  connection with the SaaS by jservice.xyz;
* distributing programs that make unauthorized changes to software;
* attempting to bypass or break any security mechanism on any of the Services,
  or using the SaaS in any other manner that poses a material security or
  service risk to jservice.xyz or any of its other customers;
* launching or facilitating, whether intentionally or unintentionally, any
  denial of service attack on any of the Services, or any other conduct that
  adversely impacts the availability, reliability, or stability of the Services;
  or altering, modifying, or tampering with the SaaS or permitting any other
  person to do the same who is not authorized by jservice.xyz;

### Network and Usage Restrictions

* restricting, inhibiting, or otherwise interfering with the ability of any
  other entity, to use or enjoy the SaaS, including posting or transmitting any
  information or software which contains a worm, virus, or other harmful
  feature, or generating levels of traffic sufficient to impede others’ ability
  to use, send, or retrieve information;
* restricting, inhibiting, interfering with, or otherwise disrupting or cause a
  performance degradation to the SaaS or any jservice.xyz host, server, backbone
  network, node or service, or otherwise cause a performance degradation to any
  jservice.xyz facilities used to deliver the SaaS; or
* interfering with computer networking or telecommunications service to any
  user, host or network, including, without limitation, denial of service
  attacks, flooding of a network, overloading a service, improper seizing and
  abusing operator privileges, and attempts to “crash” a host.

This Policy is in addition to any other terms and conditions under which
jservice.xyz provides the SaaS to you. In the event of a conflict between the
terms of this Policy and any other terms and conditions applicable to you, the
terms of this Policy shall prevail.

jservice.xyz RESERVES THE RIGHT TO NOTIFY ITS CUSTOMERS OF ANY INFORMATION THAT
AFFECTS THE SECURITY OF THE SAAS.
