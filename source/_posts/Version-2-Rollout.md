---
title: Version 2 Rollout
date: 2024-04-14 15:27:53
author: Curtis Schlak
excerpt: Eight years in the making, a new version of jservice.xyz is finally here!
---

# Version 2 Rollout!

<div class="section">
<h2 id="eight-years-in-the-making" class="title is-size-4">Eight years in the making</h2>
<div class="subtitle is-size-5">Thank you for everyone that has given feedback</div>

**jservice.xyz** (called `J!XYZ`) has launched its version 2 rollout after
eight years of providing a feature rich service for software developers around
the world.

</div>

## What's happening now?

We want to make this all better. "Better" is ambiguous AF. Here's how we measure
that:

* More ways to interact with the service
* Comprehensive information on the service
* Examples of different tech stacks implementing the same APIs
* More reliable data offerings

### More protocols

Things have changed since 2016. Not only do we have RESTful endpoints, we also
have GraphQL. J!XYZ supports these formats and protocols to provide software
developers a testing playground.

We have our eyes set next on [AsyncAPI 3.0](https://www.asyncapi.com) for the
next supported protocol.

### Better documentation

The original jservice.xyz Web site had complete documentation, but it was a
little difficult to parse. We have specifications like OpenAPI to help us
communicate the interactions with an API and the shape of its data. We should
use that!

The new rollout contains an OpenAPI 3.0.2 specification to document its
REST-based interactions. This will help developers interact with the service
better.

### More back-end implementations

In addition to that, I'm going to clean up the server-side code and provide
multiple implementations of it. Software developers wil be able to locally run
the backend of their choice.

It is my hopes that each server-side implementation can provide new backend
developers exemplary code bases for them to read and learn from.

### More often data updates

Rather than batch updating the official data on irregular intervals, we're going
to update the data importer to run on a daily basis. It will do two things.

* Reset the database to a "clean" state that keeps only official data
* Add any new data to the service

### Cleaner data

The data that powers J!XYZ comes from the Internet. The original data imports
did little to clean the data. Instead, it just relied on a naive Web scraper to
get some data. We've changed that. We now do much more in making sure the data
is well-formed and not inaccessible.

## A brief history

Back in 2016, I was teaching software developers at organizations around the
world. One of the biggest hurdles was finding robust free Web APIs to use during
the learning journeys. There was a great API named *jservice.io* (no longer
running) that provided trivia data harvested from the [Jeopardy!
Archive](https://j-archive.com/). However, it had one main problem.

<p class="has-text-centered has-text-weight-bold is-size-5">It did not provide an <code>https</code> version.</p>

That was a problem for the companies where I was teaching their proprietary
stacks that ran on `https` only. You can't make an AJAX call from a secure page
to an insecure endpoint.

[Let's Encrypt](https://letsencrypt.org/) already existed. I knew how to get
free certs. I could put it on a Linode somewhere and run the service. I would
happily do that! But, then, I ran into another problem.

<p class="has-text-centered has-text-weight-bold is-size-5">jservice.io was not free software nor open source</p>

<p class="has-text-centered is-size-1">🤯</p>

I admit: I am a little reactionary. When I see a hole in the free software
ecosystem, my first inclination is to just write some software to fill it. So, I
did. I bought a domain. I wrote a Web scraper. I wrote a simple service. I
hosted it on a little server with TLS.

It was a read-only API, where you could query for clues or categories or games.
My co-instructors Eric Schwartz and David Michael Gregg pointed out that it
would be really great if it would support _writing_, too. I spent a weekend
implementing `POST`, `PUT`, and `DELETE` methods on some of the endpoints where
they made sense.

And, that's been jservice.xyz for the past eight years.
