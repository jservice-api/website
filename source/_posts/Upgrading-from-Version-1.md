---
title: Upgrading from Version 1
date: 2024-04-19 20:56:52
author: Curtis Schlak
excerpt: A guide to updating code that you have for Version 1
---

<div class="section pb-0">
<div class="notification is-warning">

This is a major version change which means breaking changes to the API. Read on
to understand the RESTful API has now been made better.

</div>
</div>

# Upgrading from v1

It's always hard when a new major version comes of anything. You have to look at
your existing code and decide if it's even worth updating, or evaluate new
services for your work. I feel that pain. 🤕

This page is here to help you adapt anything that you may have that relies on
J!XYZ: curriculum, existing code samples, etc.

But, we think these changes are for the better. We believe they make the API
more consumable and more comprehensible.

## The hostname has changed

You used to get the API from the hostname `jservice.xyz`. That's not going to
work anymore. Instead, we have a stack-specific hostname for each service.
You'll want to use `node.jservice.xyz`, now, or any of the other implementations
that come along.

## Version 1 is still there <span class="tag is-danger is-large">deprecated</span>

If you want to use version 1, though we **do not recommend it**, you can still
access it but **only on the Node.js implementation** at `node.jservice.xyz`.

## The path has changed

For the RESTful API, instead of using `/api` as the root path prefix, that has
changed to `/rest`. This is to separate the different _types_ of APIs that J!XYZ
has. This is not a common thing to have multiple types of API formats for a
single service. But, J!XYZ is an _educational_ resource. So, for this, it makes
sense.

If the Web worked the way that we wanted it to, we would actually use [content
negotiation](https://en.wikipedia.org/wiki/Content_negotiation) to determine how
to format the result. While GraphQL has its own type (`application/graphql`),
many clients use `application/json` as the content type. At that point, there
is no way to disambiguate one expected response from the other. 🤷

New and different paths it is!

## Documented errors

There was not documentation for errors in v1. Because we've moved to OpenAPI 3
to document the API, you can browse the shape of those errors to determine how
you need to handle them.

Check out the [documentation](/documentation) page.

## No `id` field for resources

The biggest change is that _all_ resources responses no longer have an `id`
field on them. Instead, every resource has an `href` field that is the path you
should use to interact with that specific resource.

Here's a comparison of the `Category` resource between versions 1 and 2.

<div class="columns">
<div class="column">
<div class="is-size-5 has-text-weight-bold">Version 1</div>

{% codeblock lang:json highlight:true mark:2 %}
{
  "id": 123,
  "title": "\"ALLO\" DARLIN'",
  "canon": true
}
{% endcodeblock %}

</div>
<div class="column">
<div class="is-size-5 has-text-weight-bold">Version 2</div>

{% codeblock lang:js highlight:true mark:2 %}
{
  "href": "/rest/categories/123",
  "title": "\"ALLO\" DARLIN'",
  "official": true
}
{% endcodeblock %}

</div>
</div>

 Note that
the `id` value is replaced with an `href` value. The `href` is a _hypermedia_
solution, and is far more correct than just and `id`. With just the `id` field,
it is up to the programmer to figure out how to construct a URL to interact with
that resource.

## All lists are paginated

In version 1, only the list of clues resource (`/api/clues`) had pagination.
That meant that a request to the list of categories resource would return 45,734
categories every single time. That's just not feasible.

In version 2, _all_ lists are paginated with a common `Pagination` resource as
part of the list. The `Pagination` resource looks like this.

```js
{
  "previousHref": string,
  "nextHref": string,
  "total": number
}
```

The `total` value represents the total number of items available in the list. It
is merely there as a data point for you to display.

Like the `href` discussion above, the `previousHref` and `nextHref` properties
are there for you to use to go to the previous and next pages of the list.

## From `canon` to `official`

In version 1, the `Category` and `Clue` resources had a Boolean `canon` property
which indicated that the resource was loaded from an actual trusted source. All
API-created categories and clues had a value of `false` for that property.

<div class="has-text-centered has-text-weight-bold mb-4">

People had a hard time understanding what `canon` meant.

</div>

In the new API, the property is now named `official`, since it is an official
`Category` or `Clue`.

## Random game has clue data

In v1, the `RandomGame` had only a list of clue `id` and `href` properties. You
would then need to make separate calls to get each of those different `Clue`
resources.

In v2, the `RandomGame` resource now includes a list of `ClueRef` objects. These
are slim resources that contain a small but usable number of properties from the
`Clue`. That way, you can immediately use the response. If you need all of the
properties for the `Clue` resource, then you can just use the included `href`.

## `Category` now documents conflict

If you try to create a `Category` resource that already exists, then you would
get a [409
Conflict](https://developer.mozilla.org/en-US/docs/web/http/status/409). The
behavior hasn't changed, but that is now documented as a potential response
code.

## `Clue`-specific changes

The `Clue` resource has been updated to make it easier to understand and use.
These are the changes that it has undergone.

### From `question` to `prompt`

Another point of confusion in the original API was the `Clue` property
`question`.

The `question` property is now named `prompt` in the new API. And, there's
more...

### Markdown prompts

The `prompt` property of the `Clue` resource now is of type `Markdown`. It has
the following shape.

```js
{
  "markdown": string,
  "html": string
}
```

You can use the `markdown` property if you want to safely render the content.
Or, you can use the `html` property and learn how to sanitize potentially
dangerous HTML. 😉

### 409 Conflict errors

The way that you can interact with official clues has changed

#### Deleting official clues

In v1, you could soft delete official clues which incremented their
`invalidCount` property. That is no longer a thing. If you try to `DELETE` an
official `Clue`, then you will get a [409
Conflict](https://developer.mozilla.org/en-US/docs/web/http/status/409).

#### Updating official clues

In v1, you could update official `Clue` resources. You can no longer do that. If
you try to `PUT` an official `Clue`, then you will get a [409
Conflict](https://developer.mozilla.org/en-US/docs/web/http/status/409).

### Removed top-level nested resource `id` properties

In v1, the `Clue` resource had properties `categoryId` and `gameId` in addition
to properties `category` and `game`.

In v2, only the `category` and `game` properties survive, not only because we've
removed identifiers and replaced them with hypermedia references, but because
they were repeated data that exist within the nested resource.

