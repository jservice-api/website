---
title: Documentation
layout: page
---

# Documentation

You can interact with the API documentation at any of the deployed services.

##  REST API

We are _serious_ about Representational State Transfer (REST). To us, this
means that, given a start URL, the response returns everything you need to
change the state of the application.

"Changing state" of an application is more than just modifying data. When you
change the display of your front-end, that's changing application state, even if
it doesn't make a back-end call.

> A Web application consists of _all_ of the pieces: browser, application
> server, database, and all of the pieces in-between! Application state changes
> when any one piece changes.

That's why our REST API does not contain any `id` properties. People using
RESTful APIs will take `id` values and construct URLs from them to query for
more data. **Naive URL construction is an anti-pattern!** Instead, a good
RESTful API should have `href` attributes to _tell_ you which URL to interact
with. That's the backbone of REST, the use of
[Hypermedia](https://en.wikipedia.org/wiki/Hypermedia).

Each of the deployed services have a hosted copy of the most recent API
specification built from <https://gitlab.com/jservice-api/api-definitions>. You
can find links to them here.

* <https://node.jservice.xyz/docs>
